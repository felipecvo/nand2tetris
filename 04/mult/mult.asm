// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
@sum
M=0

@R1
D=M

@i
M=D

@END
D; JEQ

@R0
D=M

@END
D; JEQ

(MULTIPLY)
@sum
D=M

@R0
D=D+M

@sum
M=D

@i
MD=M-1

@MULTIPLY
D;JGT

(END)
@sum
D=M

@R2
M=D

(EXIT)
@EXIT
0;JMP
