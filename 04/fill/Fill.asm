// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
(READKBD)
  @KBD
  D=M

  @BLACK
  D; JGT

  @pressed
  M=0

  @PRINTSCR
  0; JMP

(BLACK)
  @0
  D=A-1

  @pressed
  M=D

  @PRINTSCR
  0; JMP

(PRINTSCR)
  @SCREEN
  D=A

  @current
  M=D

  @512
  D=A

  @row
  M=D

(PRINTROW)
  @row
  D=M

  @LOOP
  D; JEQ

  @16
  D=A

  @col
  M=D

(PRINTCOL)
  @pressed
  D=M

  @current
  A=M

  M=D

  D=A

  @current
  M=D+1

  @col
  MD=M-1

  @PRINTCOL
  D; JGT

  @row
  M=M-1

  @PRINTROW
  0; JMP

(LOOP)
  @READKBD
    0; JMP
