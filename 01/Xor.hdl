// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Xor.hdl

/**
 * Exclusive-or gate:
 * out = not (a == b)
 */

CHIP Xor {
    IN a, b;
    OUT out;

    PARTS:
    And(a=a, b=b, out=temp1);
    Not(in=a, out=tempa);
    Not(in=b, out=tempb);
    And(a=tempa, b=tempb, out=temp2);
    Or(a=temp1, b=temp2, out=temp3);
    Not(in=temp3, out=out);
}
